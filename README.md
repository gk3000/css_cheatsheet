# [CSS Cheat sheet](https://gk3000.gitlab.io/css_cheatsheet/)

<img src="./pics/coverimage.png">

👉 [Web page](https://gk3000.gitlab.io/css_cheatsheet/) 👈

The idea is always to break the layout down nto components and see every component separately. Then to decide if we are looking at laying out elements horizontally only in which case most like we will go with `text-align` for the text elemnts of flex for gouped elements/non-text elements. 

If we are looking at laying htem out in 2 dimensions -- both columns and several rows then the ebst choice would be grid. 

And there are some variations. 

We will try to gather all the most used patterns of layouyt here and describe them, please feel free to submit yout questions/additions via pull requests or issues section of this repo. 

Thanks! 	

> Horizontal scroller: https://horizontal-scroll-simple-css-sample.surge.sh/